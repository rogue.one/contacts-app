﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ElbaTestTask.Models
{
    public class Repository
    {
        SQLiteConnection database;
        public Repository(string databasePath)
        {
            database = new SQLiteConnection(databasePath);
            database.CreateTable<Contact>();
        }
        public IEnumerable<Contact> GetItems()
        {
            return database.Table<Contact>().ToList();
        }
        public Contact GetItem(string id)
        {
            return database.Get<Contact>(id);
        }
        public int DeleteItem(string id)
        {
            return database.Delete<Contact>(id);
        }
        public void SaveItems(ObservableCollection<Contact> items)
        {
            foreach (var contact in items)
            {
                if (contact.Id != "0")
                {
                    database.Update(contact);
                }
                else
                {
                    database.Insert(contact);
                }
            }
        }
        public string SaveItem(Contact contact)
        {
            if (contact.Id != "0")
            {
                database.Update(contact);
                return contact.Id;
            }
            else
            {
                database.Insert(contact);
                return contact.Id;
            }
        }

    }
}
