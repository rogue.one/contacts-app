﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElbaTestTask.Models
{
    public partial class Contact
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("height")]
        public double Height { get; set; }

        [JsonProperty("biography")]
        public string Biography { get; set; }

        [JsonProperty("temperament")]
        public string Temperament { get; set; }

        [JsonProperty("educationPeriod")]
        public EducationPeriod EducationPeriod { get; set; }
    }

    public partial class EducationPeriod
    {
        [JsonProperty("start")]
        public DateTimeOffset Start { get; set; }

        [JsonProperty("end")]
        public DateTimeOffset End { get; set; }
    }

}
