﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElbaTestTask.Models
{
    public interface ISQLite
    {
        string GetDatabasePath(string filename);
    }

}
