﻿using ElbaTestTask.Models;
using ElbaTestTask.Resources;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Windows.Input;
using Xamarin.Forms;

namespace ElbaTestTask.ViewModels
{
    public class LoadPageViewModel : INotifyPropertyChanged
    {
        private bool _isLoading;

        private ObservableCollection<Contact> _contactsList;

        public ICommand GetDataCommand { get; set; }
        public bool IsLoading
        {
            get { return _isLoading; }
            set { _isLoading = value; OnPropertyChanged("IsLoading"); }
        }
        public ObservableCollection<Contact> ContactsList
        {
            get { return _contactsList; }
            set { _contactsList = value; OnPropertyChanged("ContactsList"); }
        }
        public LoadPageViewModel()
        {
            GetDataCommand = new Command(GetData);
        }

        private async void GetData()
        {
            IsLoading = true;
            HttpClient client = new HttpClient();
            ApiURL url = new ApiURL();
            List<string> resources = new List<string>(new string[] { url.resource1, url.resource2, url.resource3 });
            int count = 0;

            for (int i = 0; i < resources.Count; i++)
            {
                var response = await client.GetAsync(resources[i]);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var contacts = JsonConvert.DeserializeObject<ObservableCollection<Contact>>(content);
                    ContactsList = new ObservableCollection<Contact>(contacts);
                    App.Database.SaveItems(ContactsList);
                    count += ContactsList.Count;
                    Debug.WriteLine("Прошел " + i + " источник!");

                }
                else
                {
                    Debug.WriteLine("Не удалось получить доступ");
                }

            }
            IsLoading = false;
            Debug.WriteLine("Кол-во контактов: " + count);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
