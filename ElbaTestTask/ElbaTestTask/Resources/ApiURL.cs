﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElbaTestTask.Resources
{
    public class ApiURL
    {
        public string resource1 = "https://raw.githubusercontent.com/Newbilius/ElbaMobileXamarinDeveloperTest/master/json/generated-01.json";
        public string resource2 = "https://raw.githubusercontent.com/Newbilius/ElbaMobileXamarinDeveloperTest/master/json/generated-02.json";
        public string resource3 = "https://raw.githubusercontent.com/Newbilius/ElbaMobileXamarinDeveloperTest/master/json/generated-03.json";
    }
}
